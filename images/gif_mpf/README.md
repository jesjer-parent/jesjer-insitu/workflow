# Welcome to Valhalla!!!!!!!

... wait for a few seconds (like 28 or so on a modest internet connection in St. Genis, France) for your tubular GIF to load.  Awesomeness wasn't built in a day.

![](../workflow_gif/workflow_mpf.gif "Step-by-step MPF analysis workflow")