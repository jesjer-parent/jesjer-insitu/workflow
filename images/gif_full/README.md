# Welcome to Nirvana!!!!!!!

... wait for a few seconds (like 28 or so on a modest internet connection in St. Genis, 
France) for your tubular GIF to load.  Awesomeness wasn't built in a day. And nor 
will be the full in-situ jet calibration workflow.  But I know that 
[you can do it](https://www.youtube.com/watch?v=VZ2HcRl4wSk)!

![](../workflow_gif/workflow_full.gif "Step-by-step MPF analysis workflow")