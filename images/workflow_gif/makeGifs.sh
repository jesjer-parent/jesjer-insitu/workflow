# IMG_20200828_181415.jpg     # mpf
# IMG_20200828_181601.jpg     # mpf
# IMG_20200828_181948.jpg     # mpf
# IMG_20200828_182207.jpg     # mpf
# IMG_20200828_182427.jpg     # mpf
# IMG_20200828_182607.jpg     # mpf
# IMG_20200828_182857.jpg     # mpf
# IMG_20200828_183104.jpg     # mpf
# 
# IMG_20200828_183335.jpg     # full
# IMG_20200828_184634.jpg     # full
# IMG_20200828_184913.jpg     # full
# IMG_20200828_185131.jpg     # full
# IMG_20200828_185249.jpg     # full
# IMG_20200828_185358.jpg     # full
# IMG_20200828_185434.jpg     # full
# IMG_20200828_185702.jpg     # full
# IMG_20200828_185704.jpg     # full



export RESOLUTION=1200

rm IMG*resized*jpg

sips -Z $RESOLUTION IMG_20200828_181415.jpg --out IMG_20200828_181415_resized_mpf.jpg
sips -Z $RESOLUTION IMG_20200828_181601.jpg --out IMG_20200828_181601_resized_mpf.jpg
sips -Z $RESOLUTION IMG_20200828_181948.jpg --out IMG_20200828_181948_resized_mpf.jpg
sips -Z $RESOLUTION IMG_20200828_182207.jpg --out IMG_20200828_182207_resized_mpf.jpg
sips -Z $RESOLUTION IMG_20200828_182427.jpg --out IMG_20200828_182427_resized_mpf.jpg
sips -Z $RESOLUTION IMG_20200828_182607.jpg --out IMG_20200828_182607_resized_mpf.jpg
sips -Z $RESOLUTION IMG_20200828_182857.jpg --out IMG_20200828_182857_resized_mpf.jpg
sips -Z $RESOLUTION IMG_20200828_183104.jpg --out IMG_20200828_183104_resized_mpf.jpg
sips -Z $RESOLUTION IMG_20200828_183335.jpg --out IMG_20200828_183335_resized_mpf.jpg

convert -delay 200 -loop 0 IMG*resized_mpf.jpg workflow_mpf.gif



#sips -Z $RESOLUTION IMG_20200828_184634.jpg --out IMG_20200828_184634_resized_full.jpg
sips -Z $RESOLUTION IMG_20200828_184913.jpg --out IMG_20200828_184913_resized_full.jpg
sips -Z $RESOLUTION IMG_20200828_185131.jpg --out IMG_20200828_185131_resized_full.jpg
sips -Z $RESOLUTION IMG_20200828_185249.jpg --out IMG_20200828_185249_resized_full.jpg
sips -Z $RESOLUTION IMG_20200828_185358.jpg --out IMG_20200828_185358_resized_full.jpg
sips -Z $RESOLUTION IMG_20200828_185434.jpg --out IMG_20200828_185434_resized_full.jpg
sips -Z $RESOLUTION IMG_20200828_185702.jpg --out IMG_20200828_185702_resized_full.jpg
sips -Z $RESOLUTION IMG_20200828_185704.jpg --out IMG_20200828_185704_resized_full.jpg

convert -delay 200 -loop 0 IMG*resized_full.jpg workflow_full.gif
 
