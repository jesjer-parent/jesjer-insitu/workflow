# InSitu JES Workflow

For the REANA workflow organization of the insitu JES calibration.  This is meant to serve as
an example from which to learn.  It may evolve as time goes on, but most immediately it is
meant to be forked and viewed by those engaged in the HCW2020 session on the calibration
workflow optimization.

# Join the Discussion

To facilitate learning this $*@^, we have a JES/JES workflows mattermost channel - [Link to jesjer-workflow](https://mattermost.web.cern.ch/signup_user_complete/?id=exry7bwzt7gwipemcuuo5rj47h).
Please join that and ask questions in the appropriate channel.

In addition to these resources, there is an ATLAS mattermost workspace for discussions on these tools.  I
would encourage you to join it here - [Link to atlas-ap](https://mattermost.web.cern.ch/signup_user_complete/?id=txz6dop6zjnqmcorqjw7orxx8w).  Once joining this, I encourage you to subscribe 
to the following channels :
  - `discuss-ci` : Continuous integration/development stuff in GitLab
  - `discuss-containers` : Docker and building containers in GitLab CI
  - `discuss-wflow` : Workflows and RECAST/REANA
  
These are aligned, respectively, with the three main stages of developing your re-analyzeable analysis workflow.
The discussion crowd there is super responsive and helpful.

# Background Knowledge

This is *not* self-contained and to benefit maximally from this repository, you should educate
yourself about the foundational tools that go beyond just running your analysis. These tools
are the same as those used to satisfy the [RECAST requirement](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ExoticsRECAST) for analyses in {EXOT,SUSY,HDBS}
and so if you have experience there, then you are set.  If not, then we would ask that before
diving into this, you work through the following materials in order :
  - [CICD](https://hsf-training.github.io/hsf-training-cicd/index.html) : This surrounds a crucial organizational aspect of your analysis 
  repository.  There are videos that exist that guide you through this as if you were working
  with an instructor in a bootcamp environment - [link to videos](https://www.youtube.com/watch?v=NxhDGMo9ILM&list=PLWZ1NKCZTdqcnTEx_CkfTP_3uZWcDOgxY).
  - [Docker](https://awesome-workshop.github.io/intro-to-docker/) : This is what allows us to encapsulate your entire analysis into an containerized 
  format, an ''image''.  There are videos that exist that guide you through this as if you were working
  with an instructor in a bootcamp environment - [link to videos](https://www.youtube.com/watch?v=Qr42pEtio-Q&list=PLKZ9c4ONm-VnqD5oN2_8tXO0Yb1H_s0sj).
  - [Workflows](https://awesome-workshop.github.io/reproducible-analyses) : This is the methodology by which you can compose all of the executables 
  and scripts into a single dynamic workflow.  This uses the [REANA](https://reana.cern.ch/) setting, and so the
  commands may be a bit different than highlighted here, but working through and learning
  REANA will allow you to form a sound foundation and the move to RECAST will be rather seemless.
  Please note that while working through this, there are two workflow languages presented and
  you will want to ensure that you learn the [yadage](https://yadage.readthedocs.io/en/latest/) language as this is what is used 
  for the workflows we have here.
  
These three topics were fully covered in the [Awesome Workshop](https://indico.cern.ch/event/854880/) held at CERN in February 2020
which used CMS OpenData (yeah, cringe now and then appreciate that their dataset is helping
us as a mode to learning).  This is encapsulated in the [CMS OpenData HTauTau Payload](https://hsf-training.github.io/hsf-training-cms-analysis-webpage/) for which
there are also a set of nice videos as well ([Link to Videos](https://www.youtube.com/playlist?list=PLt-F9pA2Txte8b-2YoipRoSAe5YSMA_vP)).

# Help!!!

It should finally be acknowledged that there do exist official documentation for all of these tools
as well.  They are mature and useful, particularly in the case of CICD and Docker which have originated
from outside of the HEP community.
  - [GitLab CICD](https://docs.gitlab.com/ee/ci/) : This is *the* official resource and has many examples.  If you have a question that begins
  ''... but can I do {x,y,z} in my CI?'', then please start by googling for that with *gitlab cicd* attached
  and you will probably be directed here.  If this doesn't work, then ask [Giordon Stark](mailto:gstark@cern.ch).  He's my
  goto super-expert.
  - [Docker](https://docs.docker.com/) : This is *the* official resource and has many examples.  Like the CICD thing, try asking google.
  If that doesn't work, then ask [Matthew Feickert](mailto:Matthew.Feickert@cern.ch), he's my goto super expert.
  - [RECAST Docs](https://recast-docs.web.cern.ch/recast-docs) : The official source of documentation.  This is
  now rather mature, but if there is an issue then it may be because the docs need to be improved.  For this one
  I refer you to [Danika MacDonell](mailto:danikam1@uvic.ca).
  - [RECAST Discourse](https://atlas-talk.web.cern.ch/c/recast/5) : Like StackOverflow but for RECAST.  Don't 
  be bashful about writing your ''silly question'' here.  Other's will benefit from the archived discussion that
  results.
  
## Official Contacts

In addition to the documentation and few folks listed above, it should be noted that there are additional 
''official'' ATLAS communication channels and contacts for this.  Don't be afraid to reach out to them
with technical questions.  This is new and you are pioneers for CP!
  - Analysis Preservation Contact : [Danika MacDonell](mailto:danikam1@uvic.ca)
  - EMail List : [atlas-phys-exotics-recast@cern.ch](mailto:atlas-phys-exotics-recast@cern.ch)

  
# MPF Analysis Overview

The MPF analysis uses as a reference ''object'' the full leptonic Z boson decay
plus other soft energy depositions.  The details are available in multiple publications
from [Run1](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/PERF-2012-01/) and [Run2](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/JETM-2018-05/) and what we focus on describing here is the technical *workflow*
of the analysis. By this we mean the essential bits and pieces that need to be run.  In
what sequence?  Which output files do each step produce?  Of these, which are used for 
validation and debugging of that stage?  Which are necessary to be saved and passed to the next
stage?  Answering all of these same questions for your analysis is essential to make your 
life easier.  

*NOW STOP!*

We suggest that you stop typing things now and take an hour to sit with your analysis
partner and draw a visual representation of this process.  The one for the MPF analysis
can be found below and describe below that. Honestly, stop now.  

![](images/mpf_workflow.jpg "Full MPF analysis workflow")

This image represents the following textual representation of the workflow :
  - *[Step 1]* : Perform the MPF analysis for both data and MC.
  This is done using the code in the [MPF](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF) 
  [**NOTE**: We are using the `R21` branch.] repository and performs the event 
  selection and calculation of the balance and creation of the balance spectra.  
    - Executable : [RunInSituJes](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF/-/blob/R21/source/InSituJES/util/RunInSituJES.cxx)
    - Each run requires a few config files that are all linked together.  
      - [topLevelConfig](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF/-/blob/R21/source/InSituJES/share/samConfig_mc) : This is the top config.
      - [configFile](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF/-/blob/R21/source/InSituJES/share/configFile) : This is a bit hard-coded and so requires that it be changed before each
      run manually.  This, in turn, means that we have a wrapper script [ModConfig.py](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF/-/blob/R21/scripts/ModConfig.py)
      which can dynamically change this config.  Once could perhaps imagine having command
      line arguments that would be better.
      - [selectionConfig](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF/-/blob/R21/source/InSituJES/share/Selections/Electron) : This is referenced within the topLevelConfig and governs how
      you perform the selection of your topology.
    - This outputs the standard `submitDir` which contains the `hist-XYZ.root` file and this 
    is the relevant input for the next step.  You need to preserve this hist file for `MC` and `Data`
    separately but be sure to merge all the files together appropriately and separately
    for Data and MC.
  - *[Step 2]* : The next step is to perform the analysis of the balance histograms
  created in MPF analysis section.  This is the step in which you perform Gaussian fits
  to the balance histograms and create the response curves.  This is executed using 
  the compiled code housed in the [MPF_Plotting](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF_PlottingTools) submodule and is entirely controlled by
  command line arguments.  This is done for each of the systematic variations, both from the 
  incoming CP objects as well as the topological variations, which is why you see systematics
  for data.
    - Executable : [PlotResponse](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF_PlottingTools/-/blob/master/util/PlotResponse.cxx)
    - For this, you need to point the executable at the input histogram file that came from
    the previous stage and specify which type of calibration method is being performed.  In this 
    case, you will be using the `Response` type of input.
    - The output of this portion includes two things
      - A set of validation `.pdf` files and `.root` files that constitute the actual fits of the
      balance spectra.
      - The second thing is the initial input file itself. That is because the executable actually
      augments the input file by adding a graph which itself represents the synthesized balance.
      This will serve as the input to the next step.
  - *[Step 3]* : The next step is to perform the synthesize of the outputs to find the mapping
  from the input reference system pt to the uncalibrated jet pt.  This is executed using 
  the compiled code housed in the [MPF_Plotting](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF_PlottingTools) submodule and is entirely controlled by
  command line arguments.  This is done for each of the systematic variations, both from the 
  incoming CP objects as well as the topological variations, which is why you see systematics
  for data.
    - Executable : [Mapping](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF_PlottingTools/-/blob/master/util/Mapping.cxx)
    - For this, you need to point the executable at the input histogram file which is the output
    from the previous step.  That is, the one that has the first graph of (pt, balance) added to it.
    - The output of this portion includes two things
      - A set of validation `.pdf` files and `.root` files that constitute the calculated mapping.
      - The second thing is the initial input file itself. That is because the executable actually
      augments the input file by adding a graph which itself represents the synthesized balance.
      This will serve as the input to the next step.
  - *[Step 4]* : The next step is to perform the gathering of the response curves for all of the
  systematic variations.  This is executed using 
  the compiled code housed in the [MPF_Plotting](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF_PlottingTools) submodule and is entirely controlled by
  command line arguments.
    - Executable : [doSyst](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF_PlottingTools/-/blob/master/util/doSyst.cxx)
    - For this, you need to point the executable at a few input histogram files which have gone through
    the `PlotResponse` and `Mapping` steps as well as a few command line arguments.
      - `--data` : The input data file that has been augmented.
      - `--MC` : The input MC file that has been augmented.
      - `--altMC` : The input MC file that has been augmented, but for the alternate MC which provides the theory systematic.
      - `--jetAlg` : Like `AntiKt4EMPFlow`, the normal jet algorithm name.
      - `--calibType` : This is tied to the analysis itself and is `Zjet` nominally.  This seems to be only marginally used and should probably be looked into to be modified in the future.
      - `--measurement` : This refers to whether it is the MPF (`mpf`) or Direct Balance (`bal`) method.
    - The output of this portion includes three things
      - `syst_JES.root`           : The raw balance data file with the nominal binning, this will be passed to the next stage.
      - `rebinned.root`           : The *rebinned*  balance data file with the nominal binning, this will be passed to the next stage.  This is the one that is "actually" used.
      - `mpf_Ratio_INTERNAL.pdf`  : A validation plot showing the binned balance comparison in MC and data
  - *[Step 5]* : The next step is to perform the final gathering and organization of the synthesized
  systematic variation graphs into "The Steven Schramm Plot" (you know the one I'm talking about, come on ... yeah, there it is).  
  This is executed using the compiled code housed in the [MPF_Plotting](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF_PlottingTools) submodule and is entirely controlled by
  command line arguments.
    - Executable : [plotSyst](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF_PlottingTools/-/blob/master/util/plotSyst.cxx)
    - For this, you need to point the executable at a single input file which is one of the two outputs from the previous
    `doSyst` step using the `--file` argument.
    - The output of this portion includes two things
      - `Systs_INTERNAL.pdf`   : "The Steven Schramm Plot" which shows the final results of this analysis.
      - `TotalSyst.root`       : The root file with the properly formatted inputs to be passed to the calibration combination stage later on.

If you would like to see this full workflow played out in a tubular GIF, then check this out - [Link to Tubular GIF](./images).  
It may take a few seconds to load once you go there so be patient.

# Running the InSitu Workflow

Now that you have spent some time understanding this analysis, and hopefully reflected on
your own analysis, it is time to start running the workflows provided here.  Much of what 
is below is little more than contextualization of the [full RECAST doc](https://recast-docs.web.cern.ch/recast-docs/workflowauthoring/intro/) examples in terms
of what is provided here, so again, we encourage you to read those!

## Available MPF Example Workflows
There are a number of workflows here that build in order of perceived complexity.  Hopefully
you can use them to apply a simple workflow for your analysis and then scale up little by
little.
  - `workflow_mpf_single_local`            : This is the simplest workflow, it takes as input a single input
  file that is stored locally on the machine you are working on and runs only the `RunInSituJES` part of the
  workflow described above for this one file.  In what is included here, it was written on Sam's laptop
  and so has the input file placed in `/Users/meehan/work/InSitu/`.  You will likely need to change this, perhaps
  along with the files.  However, the same input files are available for anyone on EOS, as described below.
  - `workflow_mpf_double_local`            : This performs a slight modification of `workflow_mpf_single_local` and demonstrates
  how two jobs can be run in parallel on different files but using the same executable.  You'll eventually need
  to parallelize the processing of all the inputs and this is the first move in that direction.
  - `workflow_mpf_single_eos`              : This performs a slight modification of `workflow_mpf_single_local` and instead of
  reading the file from the local `/Users/meehan/work/InSitu/` directory, it streams files directly from EOS from the `jesjer`
  service account space `root://eosuser.cern.ch//eos/user/j/jesjer/MPF/`.  This requires authentication and it is
  currently done in a sloppy way that should be improved in the future.  You can't store all the many Tb of derivations
  locally and EOS is the place to store stuff for the time being.
  - `workflow_mpf_multistep_mc`            : This is a bit of a jump from the previous workflows.  This time, we are going
  to run jobs using whats called a [multistep-stage](https://awesome-workshop.github.io/reproducible-analyses/07-higgstotautau-parallel/index.html) which will be a useful concept for when you eventually need to distribute
  your EventLoop processing jobs.  At the end, we (1) collect these outputs, (2) merge them into a single file, and (3) 
  plot one of the output spectra.
  - `workflow_mpf_multistep_mc_data`       : This builds slightly on `workflow_mpf_multistep_mc` but also performs a runs job
  in parallel that processes the `data18` sample (alright, only 3 files).  This then allows us to make our first automated
  data/MC comparison plot for validation.
  - `workflow_mpf_multistep_mc_data_fits`  : This is the mac-daddy workflow and builds on `workflow_mpf_multistep_mc_data` to
  run the ''post-processing'' code that does all the fitting and systematic synthesis.  This is the workflow that
  will give you your ''Steven Schramm Plot''.

## Test Input Files

The test files that are used for the above example workflows are stored in the `jesjer` service account
on EOS.  The entirety of files which are used in this example are :

*Data*
```
root://eosuser.cern.ch//eos/user/j/jesjer/MPF/data18_13TeV.00356177.physics_Main.deriv.DAOD_JETM3.f956_m2004_p4061/DAOD_JETM3.20294446._000024.pool.root.1
root://eosuser.cern.ch//eos/user/j/jesjer/MPF/data18_13TeV.00356177.physics_Main.deriv.DAOD_JETM3.f956_m2004_p4061/DAOD_JETM3.20294446._000142.pool.root.1
root://eosuser.cern.ch//eos/user/j/jesjer/MPF/data18_13TeV.00356177.physics_Main.deriv.DAOD_JETM3.f956_m2004_p4061/DAOD_JETM3.20294446._000216.pool.root.1
```
*MC*
```
root://eosuser.cern.ch//eos/user/j/jesjer/MPF/mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_JETM3.e3601_e5984_s3126_r10724_r10726_p4060/DAOD_JETM3.20277559._000025.pool.root.1
root://eosuser.cern.ch//eos/user/j/jesjer/MPF/mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_JETM3.e3601_e5984_s3126_r10724_r10726_p4060/DAOD_JETM3.20277663._000082.pool.root.1
```

### Digression : Service Accounts (`jesjer`)

The concept of the "Service Account" is one that is foreign to many people.  These are accounts that 
can be requested for any official purpose within CERN.  Once approved, they function *identically* to
your standard account.  You can log onto lxplus with them.  You can request `afs` and `EOS` space, as
well as have a CERNBox instance with them.  You can even host webpages through them.  If at any point 
you leave CERN and your computing account is shut down, they they will defer the account ownership
to whomever is listed as your supervisor.  In this sense, they are persistent in a way that user accounts
are not guaranteed to be.

They provide two primary benefits :
  - *Credentials* : The user account comes with its own `<username>` and `<password>` which are
  entirely divorced from your personal credentials.  In this way, they can be shared amongst your
  analysis team and used for CI/CD authentication or other such purposes.
  - *Persistence* : As described above, when someone leaves CERN, their user account gets wiped.  But the
  service accounts under their name automatically defaults to their supervisor.  In this way, the work
  under these accounts is never lost.  Eventually, if everyone left, it would simply belong to the director
  general.  And come on, I'm sure that they would still want to be able to run the ATLAS jet calibration!
  
It is highly encouraged to use service accounts for each project you participate in.  You can request them
here - [Link to Request Service Account](https://account.cern.ch/account/Management/NewAccount.aspx).

For these purposes, we have the following account :
  - `<username>` : jesjer
  - `<password>` : <first name of your spanish convener, all lowercase>-<the worst year in modern history>

## Setup

Before you begin, you need to ensure you have a few essential tools.  This assumes that you have
actively worked through the tutorials listed above and know how to use git and docker, meaning 
that you have docker installed.  

### Get RECAST
Start by installing RECAST on your local machine as described in [the RECAST docs](https://recast-docs.web.cern.ch/recast-docs/workflowauthoring/intro/#initial-setup)
```
pip install recast-atlas
```
and validate that this worked appropriately by running your first workflow
```
recast run examples/rome --backend docker
```
This should take a couple minutes and is an example from the Exotics 
workshop in Rome a few years back which is doing a reinterpretation
of a BSM search.  You should see some stuff printing out about
and `<eventselection>` and then a `<statanalysis>` :
```
2020-08-28 19:23:39,506 |     yadage.wflowview |   INFO | added </eventselection:0|defined|unknown>
2020-08-28 19:23:39,829 |     yadage.wflowview |   INFO | added </statanalysis:0|defined|unknown>
```
it will then run for a while and chug through the reinterpretation, eventually
printing out the results for you.
```
2020-08-28 19:28:11,681 |  yadage.steering_api |   INFO | visualizing workflow.
2020-08-28 21:28:12,148 | recastatlas.subcomma |   INFO | RECAST run finished.

RECAST result examples/rome recast-f714da94:
--------------
- name: CLs 95% based upper limit on poi
  value:
    exp: 0.8924846399964371
    exp_m1: 0.6377501820447065
    exp_m2: 0.4731739008380644
    exp_p1: 1.2720762961732819
    exp_p2: 1.7545752712294322
    obs: 1.3352971254860764
- name: CLs 95% at nominal poi
  value:
    exp: 0.25999040745937085
    exp_m1: 0.10547655600578199
    exp_m2: 0.03889040527686523
    exp_p1: 0.5345040672498215
    exp_p2: 0.8276574946063575
    obs: 0.574709475331039

Samuels-MBP:InSitu meehan$
```

### Get the Code

This is where things get a bit funky (at least for Sam).  We are going to get the code
to enable us to run, and develop, the MPF workflow.  However, we aren't going to be
actually working with any of the MPF code itself.
```
git clone ssh://git@gitlab.cern.ch:7999/jesjer-parent/jesjer-insitu/workflow.git
```
This is because we are going to be *orchestrating the use of the MPF code*, not 
actually writing it.  We will be doing __declarative__ programming while writing
these workflows, as opposed to __imperative__ programming which is the way we are
normally used to thinking when working on our own analysis.  (Aside : If you
worked through the [Workflows Tutorial](https://awesome-workshop.github.io/reproducible-analyses), this will sound familiar ... come on, go
back and spend a day if you haven't already.).

## Running A Workflow

Now that we have the analysis, let's run the simplest workflow - `workflow_mpf_single_local`.
The first step in being able to run a workflow is ensuring that RECAST "knows" about it.
this is done via the `recast catalogue` - only workflows registered in the catalogue can
be run.  Start by examining which workflows RECAST currently knows about
```
recast catalogue ls
```
you should see a few default workflows for examples as well as that for `atlas/atlas-conf-2018-041`,
which is the [SUSY multi-bjet search](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2018-041/) reinterpretation.

Now add your workflow
```
$(recast catalogue add workflow/workflow_mpf_single_local)
```
You are pointing the catalogue at the directory where the necessary workflows files (`recast.yml`,`steps.yml`,`workflow.yml`) exist.
The syntax for this seems strange and looks like some funky environment variable thing, right? Yeah I know ... check out
[the nice discourse discussion on this point](https://atlas-talk.web.cern.ch/t/why-is-the-needed-in-recast-catalogue-add-path-to-repo-to-set-the-catalogue-path/105) - get engaged in the communication methods for this tool!
Now reinspect the list of workflows that RECAST knows about (`recast catalogue ls`).  You should now see a new
workflow called `mpf/single_local` which has a short description that let's you know
it pertains to your MPF workflow.  This has been registered via the presence of the 
`recast.yml` file in the directory you pointed at (`workflow/workflow_mpf_single_local`) and if you look at this file, you
will indeed see that the name of this workflow is `name: mpf/single_local`.

Now let's run it.  This time we refer to it by its recognized name in RECAST, not the local directory where it lives.
This time, we are going to use a `--tag` so that the output directory has a sensible name.
```
recast run mpf/single_local --backend docker --tag jetsrock-0
```

... unless you have gone in and done some digging, then this will probably fail.  The reason
being is the input files.  Recall that they were stored in `/Users/meehan/work/InSitu/` when I was
developing this.  This parameter is stored in the `recast.yml` file as the `initdir` parameter and you should change it to be 
whatever you have locally.  If you are wondering what this parameter is and what the structure of this 
`recast.yml` file is, please [read the official RECAST docs](https://recast-docs.web.cern.ch/recast-docs/workflowauthoring/running/). 

... alright, now that this is fixed, try rerunning the workflow.  As it runs, look
for the following bits and pieces in the interactive log :

the `eventselection` job is added to the graph of your workflow
```
2020-08-28 19:54:48,829 |     yadage.wflowview |   INFO | added </eventselection:0|defined|unknown>
```
the `eventselection` node has been successfully created in all its glory with all of the input parameters evaluated
to be proper and valid values that you provided
```
2020-08-28 19:54:49,257 |    adage.pollingexec |   INFO | submitting nodes [</eventselection:0|defined|unknown>]
```
the `eventselection` node has been successfully run (i.e. you have processed the input MC files)
```
2020-08-28 19:55:54,044 |           adage.node |   INFO | node ready </eventselection:0|success|known>
```
the same three lines for the `validate` portion of the workflow that is a little job that plots the
`hJetPT` spectrum.  Note how this node/job was not created until the `eventselection` job successfully finished
```
2020-08-28 19:55:54,188 |     yadage.wflowview |   INFO | added </validate:0|defined|unknown>
2020-08-28 19:55:54,518 |    adage.pollingexec |   INFO | submitting nodes [</validate:0|defined|unknown>]
2020-08-28 19:56:00,934 |           adage.node |   INFO | node ready </validate:0|success|known>
```
digesting this simple workflow in a way that internalizes the concept of a directed acyclic graph
will be critical for you to being able to appreciate what is happening "under the hood".

### What to Look For

Now that your job has finished successfully, let's take a look at the output, all of which
can be found in the directory `recast-jetsrock-0`.  You will see three directories :
  - `_yadage` : This contains the meta information about the entire workflow globally.
  - `eventselection` : This contains the information pertaining to the execution of that one node.
  Recall that we noted in our visual workflow that we need to pass the output `hist-XYZ.root` file
  to the next stage of the analysis.  Well, that's what you see as `powpythia.root`.  Dig into the 
  `specs/workflow.yml` and `specs/steps.yml` file to see why its called this instead of `hist-XYZ.root`.
  The fact that this file shows up here is due to it being [published](https://recast-docs.web.cern.ch/recast-docs/workflowauthoring/workflow/#parameter-references) by the job.
  - `validate` : This is not shown on our visual workflow but serves as a simple "exercise for the reader".  
  You should see in this directory a `.pdf` file which is a pT spectrum, albeit with low stats.  This
  has been published by the job and is drawn based on the input from the `eventselection` job.  Again, dig into the 
  `specs/workflow.yml` and `specs/steps.yml` and see if you can discern how on job passes this file to the second job
  and where the validation plot is published.
  
# Hoo Ra Ra

And that's it, you have now run your first JES workflow.  You've "only" made a really crappy pT
spectrum, but its a start.  Take some time to play around with the additional workflows that you
have at your disposal.  If they break, then ask someone for help.  The error messages are not
so illuminating and the documentation, although much better than one year ago, is still a work in progress.

If you are able to run the `workflow_mpf_multistep_mc_data_fits` and get the Steven Schramm Plot out
the other side, then this is [big news](https://www.youtube.com/watch?v=9U4Ha9HQvMo).  In all seriousness though
doing so is a first in many respects, so let us know!  Next, reflect on how the `workflow_mpf_multistep_mc_data_fits` workflow
is illustrated in the [tubular gif](./images/gif_mpf).

And finally, if you have made it here, spend some time reflecting on the [bodacious gif](./images/gif_full) and
imagining how we can make that a reality.

# Amassing a Workflow Army
In addition to these, workflows are being written for all of the individual analyses and stored here
in a public way to allow for REANA to access the images as needed.  Documented here are :
  - The bits and details of the various analyses that are essential to start (e.g. repo location, branch)
  - The hacks that have been made along the way that may have to be reverted when merging in with official work
  - The workflows that exist as starting points for these analyses



## MCCalib IsolateJetTree Making
### Essential details : 
  - JetEtMiss Expert : [Nathan Lalloue]() [Pierre-Antoine Delsart]()
  - Official Repo : [atlas-jetetmiss-jesjer/MCcalibrations/IsolatedJetTree](https://gitlab.cern.ch/atlas-jetetmiss-jesjer/MCcalibrations/IsolatedJetTree)
  - Link to Local Repo : [jesjer-parent/jesjer-insitu/IsolatedJetTree](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/IsolatedJetTree)
  - Branch : `master`
  - Works with release : `AnalysisBase,21.2.113`
  
### Modifications/hacks implemented : 
  - Reorganized the package to have a source and build directory
  - hack2
  
### __TODO__ 
  - Get the code to work with the `xAH` base images that exist
  - Remove the large `.root` files in the repository

### Workflows :
  - `workflow1`
  - `workflow2`


## Pileup Subtraction
### Essential details : 
  - JetEtMiss Expert : 
  - Official Repo : 
  - Link to Local Repo : 
  - Branch : 
  - Works with release : 
  
### Modifications/hacks implemented : 
  - hack1
  - hack2
  
### __TODO__ 
  - todo1
  - todo2

### Workflows :
  - `workflow1`
  - `workflow2`
  
  
## Absolute MC Calibration
### Essential details : 
  - JetEtMiss Expert : [Nathan Lalloue]() [Pierre-Antoine Delsart]()
  - Official Repo : 
  - Link to Local Repo : 
  - Branch : 
  - Works with release :
  
### Modifications/hacks implemented : 
  - hack1
  - hack2
  
### __TODO__ 
  - todo1
  - todo2

### Workflows :
  - `workflow1`
  - `workflow2`
  
## Global Sequential Calibration
### Essential details : 
  - JetEtMiss Expert : [Nathan Lalloue]() [Pierre-Antoine Delsart]()
  - Official Repo : 
  - Link to Local Repo : 
  - Branch : 
  - Works with release : 
  
### Modifications/hacks implemented : 
  - hack1
  - hack2
  
### __TODO__ 
  - todo1
  - todo2

### Workflows :
  - `workflow1`
  - `workflow2`

## Eta Intercalibration
### Essential details : 
  - JetEtMiss Expert : [Katherine Dunne](), [James Grundy](), [Tae Hyoun Park]()
  - Official Repo : [atlas-jetetmiss-jesjer/Insitu/Dijet-Insitu-Calibration-and-Resolution](https://gitlab.cern.ch/atlas-jetetmiss-jesjer/Insitu/Dijet-Insitu-Calibration-and-Resolution)
  - Link to Local Repo : [jesjer-parent/jesjer-insitu/Dijet-Insitu-Calibration-and-Resolution](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/Dijet-Insitu-Calibration-and-Resolution)
  - Branch : [largeR_JG_main](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/Dijet-Insitu-Calibration-and-Resolution/-/tree/largeR_JG_main)
  - Works with release : `AnalysisBase,21.2.51`
  
### Modifications/hacks implemented : 
  - Make the first stage analysis `EtaInterCalxAODAnalysis` run over filelists instead of comma-delimited lists
  
### __TODO__ 
  - Remove all of the `.root` files from the repository.  Need to determine how to best import the pileup reweighting stuff into an analysis.

### Workflows :
  - `workflow_etaintercalibration` : This is the workflow ''in progress'' but will be built into the full workflow.  As such, it includes
    the ntuple production and all synthesis/post-analysis stages. If you need more basic workflows that do only
    single steps, please refer to the `MPF` analysis which sequentially develops workflows to be more complicated.
  - `workflow_etaintercalibration_grid` : This workflow is an example of reading in files using GRID-accessed resources after authrnetication
    with a grid certificate.
  - `workflow_etaintercalibration_reana` : This workflow is the same as the `workflow_etaintercalibration` but with modifications that allow
    it to run on the `REANA` system.  To use it, you will have to do a bit of educating here [https://reana.cern.ch](https://reana.cern.ch).  
    And if you are outside of CERN and find that you can't get to that site, then you need to tunnel in via [these directions from CERN-IT](https://security.web.cern.ch/recommendations/en/ssh_browsing.shtml).

## MPF
### Essential details : 
  - JetEtMiss Expert : [Daniel Camarero](), [Sundeep Singh]()
  - Official Repo : [atlas-jetetmiss-jesjer/Insitu/MPF](https://gitlab.cern.ch/atlas-jetetmiss-jesjer/Insitu/MPF)
  - Link to Local Repo : [jesjer-parent/jesjer-insitu/MPF](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF)
  - Branch : [R21](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/MPF/-/tree/R21)
  - Works with release : `AnalysisBase,21.2.74`
  
### Modifications/hacks implemented : 
  - `ModConfig` script to change the base configuration file
  - `PlotResponse` executable from MPF_PlottingTools picks up the mean and RMS (not the fits) so that things work with low stats

### __TODO__ 
  - todo1
  - todo2

### Workflows :
  - `workflow_mpf_single_local`            : This is the simplest workflow, it takes as input a single input
  file that is stored locally on the machine you are working on and runs only the `RunInSituJES` part of the
  workflow described above for this one file.  In what is included here, it was written on Sam's laptop
  and so has the input file placed in `/Users/meehan/work/InSitu/`.  You will likely need to change this, perhaps
  along with the files.  However, the same input files are available for anyone on EOS, as described below.
  - `workflow_mpf_double_local`            : This performs a slight modification of `workflow_mpf_single_local` and demonstrates
  how two jobs can be run in parallel on different files but using the same executable.  You'll eventually need
  to parallelize the processing of all the inputs and this is the first move in that direction.
  - `workflow_mpf_single_eos`              : This performs a slight modification of `workflow_mpf_single_local` and instead of
  reading the file from the local `/Users/meehan/work/InSitu/` directory, it streams files directly from EOS from the `jesjer`
  service account space `root://eosuser.cern.ch//eos/user/j/jesjer/MPF/`.  This requires authentication and it is
  currently done in a sloppy way that should be improved in the future.  You can't store all the many Tb of derivations
  locally and EOS is the place to store stuff for the time being.
  - `workflow_mpf_multistep_mc`            : This is a bit of a jump from the previous workflows.  This time, we are going
  to run jobs using whats called a [multistep-stage](https://awesome-workshop.github.io/reproducible-analyses/07-higgstotautau-parallel/index.html) which will be a useful concept for when you eventually need to distribute
  your EventLoop processing jobs.  At the end, we (1) collect these outputs, (2) merge them into a single file, and (3) 
  plot one of the output spectra.
  - `workflow_mpf_multistep_mc_data`       : This builds slightly on `workflow_mpf_multistep_mc` but also performs a runs job
  in parallel that processes the `data18` sample (alright, only 3 files).  This then allows us to make our first automated
  data/MC comparison plot for validation.
  - `workflow_mpf_multistep_mc_data_fits`  : This is the mac-daddy workflow and builds on `workflow_mpf_multistep_mc_data` to
  run the ''post-processing'' code that does all the fitting and systematic synthesis.  This is the workflow that
  will give you your ''Steven Schramm Plot''.

## InsituBalance 
### Essential details : 
  - JetEtMiss Expert : [Ben Honan]()
  - Official Repo : [atlas-jetetmiss-jesjer/Insitu/InsituBalance](https://gitlab.cern.ch/atlas-jetetmiss-jesjer/Insitu/InsituBalance)
  - Link to Local Repo : [https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/InsituBalancejesjer-parent/jesjer-insitu/InsituBalance](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/InsituBalance)
  - Branch : [bhonan-largeR_DB](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/InsituBalance/-/tree/bhonan-largeR_DB)
  - Works with release : `AnalysisBase,21.2.113`
  
### Modifications/hacks implemented : 
  - Considerable reorganization of the repository to move all of the source code into 
  `source` and be at the same level as the submodules
  - The default configuration for the EGamma NP configuration was changed from `FULL` 
  (150 NP) to `1NP` (6 NP).
  - Wrote simplified version of the MC systematics merging script - [removeDuplicateNominal_Single.py](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/InsituBalance/-/blob/bhonan-largeR_DB/scripts/plotting/removeDuplicateNominal_Single.py).  
  This removes the nominal directory from two of the input files before merging so there is no conflict.
  - Importing a local version of `JES_ResponseFitter` as a submodule from 
  [jesjer-parent/jesjer-insitu/JES_ResponseFitter](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/JES_ResponseFitter) 
  to allow for local hacks which facilitate running on lower stats
  - `PlotResponse` has a hack which allows for running on low stats.  Instead of 
  performing fits, you can set the `roughEstimateFit` option, which is now set by 
  default and it will take the mean and rms instead of performing an actual fit
  - In the `runMJBHists.py` step, the default pt bounds are lowered to be more reasonable.  
  This should be configurable I would think.

### __TODO__ 
  - todo1
  - todo2

### Workflows :
  - `workflow_insitubalance_single_eos`                 : Does the event selection step 
  for only one MC file stored on EOS.
  - `workflow_insitubalance_multistep_mc`               : Processes suite of MC from EOS. 
  This is only done for the `Nominal` configuration and so will not work for further processing.
  - `workflow_insitubalance_multistep_mc_data`          : Processes the data and MC to 
  make a first comparison of data/MC. This is only done for the `Nominal` configuration 
  and so will not work for further processing.
  - `workflow_insitubalance_multistep_mc_data_fullsyst` : This does everything. A suite 
  of MC is processed, along with data.  The MC are then merged together.  In both cases, 
  all of the necessary systematic variations are run.  For the MC, this is a bit involved 
  since it requires running three separate times for {`CP`,`JVT`,`EvSel`} configurations 
  separately.  This is to ensure that the memory footprint of a single job is not large and
  is analogous to what is done in grid running.  These outputs must be merged together *after*
  removing the `Nominal` output directory from two of them.  There is a special script to do
  this.  For data, you can run with `All` just fine.  These outputs need to then be reorganized
  in a special way and fed as input to the `runMJBHists.py` script which produces all of the 
  necessary validation and output `.root` files.


## Combination
### Essential details : 
  - JetEtMiss Expert : [Eva Hansen](), [Juno Nindhito](), [Bogdan Malaescu]()
  - Official Repo : [ehansen/ToolsJEScombination](https://gitlab.cern.ch/ehansen/ToolsJEScombination)
  - Link to Local Repo : [jesjer-parent/jesjer-insitu/ToolsJESCombination](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/ToolsJESCombination)
  - Branch : `master`
  - Works with release : This package requires raw root, but none of the images that are 
  available on the [dockerhub/rootproject](https://hub.docker.com/u/rootproject) space
  seem to work.  As such, a dedicated image has been build and is housed in [jesjer-parent/jesjer-insitu/docker-root](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/docker-root).
  
### Modifications/hacks implemented : 
  - `std::` was added to a few places in the code for prefixing `ostream`, `clog`, and `string`
  - In a number of places where `TString` was passed with the `this` member which analysis base did not like.  This has been modified to fix the compilation error.
  - The preprocessing step scripts were modified to take input paths.  
  
### __TODO__ 
  - The preprocessing scripts are very hardcoded.  They should be modified to take in lists of systematic variations and input/output paths instead.
  - The plethora of `xml` files that are the inputs should be removed from the repository and moved to EOS
  - The `xml` configuration files for the final statistical combination stage need to be modified or "hacked" so that the input and output paths are changed.  These should instead be arguments to the executable or something.  

### Workflows :
  - `workflow1`
  - `workflow2`




  
